const customDom = {

    getAllElements: () => document.getElementsByTagName('*'),
    getById: () => document.getElementById(id),
    createElement: (tagName) => document.createElement(tagName),
    append: (parent, child) => parent.appendChild(child),
};
