document.addEventListener('DOMContentLoaded', onReady);

function onReady() {
    const  conteiners = document.getElementsByClassName('main-menu');
    if (!conteiners.length === 0) {
        alert('Ничего не найдено');
return;
    }
    const parent = conteiners.item(0);
    let refElem = null;
    for (let li of parent.children) {
        if (li.innerHTML === '1.') {
            refElem = li;
            break;
        }
    }

    if (refElem === null) {
        console.error('Не найден элемент с поисковым значением...');
        return;
    }


    const newLi = document.createElement('li');
    newLi.innerHTML = 'new element!';

    parent.insertBefore(newLi, refElem);

}

