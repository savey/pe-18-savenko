/* ЗАДАНИЕ - 3
* Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
* */

// let sum=0;
// const sumNumber = function (...numbs) {
//     for (let i = 0; i < numbs.length; i++) {
//         sum = sum + numbs[i]
//     }
// }
//
// sumNumber(12, 13, 14, 15, 16, 17)
//
// console.log(sum);

// стрелочная функция
// let sum=0;
// const sumNumber = (...numbs) => {
//     for (let i = 0; i < numbs.length; i++) {
//         sum = sum + numbs[i]
//     }
// }
//
// sumNumber(12, 13, 14, 15, 16, 17)
//
// console.log(sum);


/* ЗАДАНИЕ - 4
* Написать СТРЕЛОЧНУЮ функцию, которая выводит переданное ей аргументом сообщение, указанное количество раз.
* Принимает два аргумента - само сообщение и число сколько раз его показать.
* Если первый аргумент(сообщение) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу - "Empty message"
* Если второй аргумент(количество раз) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу значение 1.
* */


// const userFunction = (userMassage = 'Empty message', counter = 1) => {
//     for (let i = 0; i < counter; i++) {
//         console.log(userMassage);
//     }
// };
//
// userFunction('один', 4);


/* ЗАДАНИЕ - 5
* Написать СТРЕЛОЧНУЮ функцию, которая возвращает максимальный переданый ей аргумент.
* Т.е. функции может быть передано потенциально бесконечное количество аргументов(чисел),
* вернуть нужно самый большой из них.
* */

// let maxNumb;
// const myFunction = (...numbs) => {
//     maxNumb = Math.max(...numbs);
//     return maxNumb;
// };
//
// myFunction(2,4,6,2,4,8,4);
// console.log(maxNumb);

// let maxNumb2;
// const myFunction2 = (...numbs2) => {
//     maxNumb2 = Math.max(...numbs2);
//     return maxNumb2;
// };
//
// myFunction2(2,4,6,2,4,8,4);
// console.log(maxNumb2);


const fruitShop = {
    items: {
        banana: 5,
        orange: 1,
        apple: 2,
        pineapple: 0,
        wetermelon: 7,
        kivi: 8,
        lemon: 4,
        melon: 0
    },


    functions: {
        getFruit(fruit, value) {
            if (fruit.toLowerCase() in this.items) {
                if (this.items[fruit] < value) {
                    console.error('No such fruts in shop')
                } else {
                    this.items[fruit] -= value;
                }
            } else {
                console.error('No such fruts in shop')
            }
        }
        // setFruit
    }
}

fruitShop.getFruit('banana', 12);