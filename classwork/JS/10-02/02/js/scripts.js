document.addEventListener('DOMContentLoaded', function () {
    const btn = document.getElementById('sendMassage');
    if (!btn) {
        console.error('ошибка');
        return;
    }

    btn.setAttribute('disabled', 'disabled');
    btn.addEventListener('click', onSendButtonClick);
    btn.removeAttribute('disabled');
});

function onSendButtonClick() {
    const firstName = document.getElementById('firstName');
    const phone = document.getElementById('phone');
    if (!firstName || !phone) {
        console.error('error2');
        return;
    }
    const data = {
        firstName: firstName.value,
        phone: phone.value,
    };
    console.log(data);
}
