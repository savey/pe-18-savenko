// Задаем переменные
let numberOne = 0;
let numberTwo = 0;
let mathOperation;

// Спрашиваем первое число
do {
    numberOne = prompt("Введите первое число", numberOne);
} while (isNaN(numberOne));

// Спрашиваем второе число
do {
    numberTwo = +prompt("Введите второе число", numberTwo);
} while (isNaN(numberTwo));

// Спрашиваем оператор и проверяем чтоб введенное значение было оператором из перечня в условии задачи
do {
    mathOperation = prompt("Введите математическую операцию которую нужно выполнить. Возможно выполнение только операций: +, -, *, /", '');
    if (mathOperation === '/' || mathOperation === '*' || mathOperation === '+' || mathOperation === '-') {
        break;
    }
} while (true);

// Выводим результат вычисления в консоль
console.log(calcMathOperation(numberOne, numberTwo, mathOperation));

// Создаем функцию проверки оператора и вычисления результата по переданным в функцию аргументам
function calcMathOperation() {
    if (mathOperation === '/') {
        return numberOne / numberTwo
    } else if (mathOperation === '*') {
        return numberOne * numberTwo
    } else if (mathOperation === '+') {
        return numberOne + numberTwo
    } else {
        return numberOne - numberTwo
    }
}