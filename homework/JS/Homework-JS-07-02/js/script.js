let numberOfArrayElements = 0;
let massivElemets = [];

do {
    numberOfArrayElements = prompt('Сколько элементов будет в массиве?', numberOfArrayElements);
}
while (isNaN(numberOfArrayElements));

for (let i = 0; i < numberOfArrayElements; i++) {
    let itemElement = prompt('Введите элемент массива', '');
    massivElemets.push(itemElement);
}
;

const listArray = massivElemets.map(list => `<li> ${list}</li>`).join('');
let separateAndAdd = `<ol> ${listArray} </ol>`;
document.getElementById('container').innerHTML = separateAndAdd;
let seconds = 10;

function timeSec() {
    if (seconds < 1) {
        document.body.innerHTML = '';
    } else {
        document.getElementById('timer').innerHTML = seconds;
        setTimeout(timeSec, 1000);
        seconds--;
    }
}

timeSec();
