function createNewUser() {
    this.firstName = prompt('Enter First Name', '');
    while (this.firstName === '') {
        this.firstName = prompt('Enter First Name again', '');
    }

    this.lastName = prompt('Enter Last Name', '');
    while (this.lastName === '') {
        this.lastName = prompt('Enter Last Name again', '');
    }

    this.getLogin = function () {
        this.loginNew = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return this.loginNew;
    };

    do {
        getBirthday = prompt('Enter Birthday', 'dd.mm.yyyy');
        parsBirthDay = parseInt(getBirthday.substring(0, 2));
        parsBirthMonth = parseInt(getBirthday.substring(3, 5));
        parsBirthYear = parseInt(getBirthday.substring(6, 10));
        if (parsBirthDay <= 31 && parsBirthDay >= 1 && parsBirthMonth <= 12 && parsBirthMonth >= 1 && parsBirthYear <= 9999 && parsBirthYear >= 1) {
            break;
        }
    } while (true);

    this.birthday = new Date(parsBirthYear, parsBirthMonth - 1, parsBirthDay);
    today = new Date();

    this.getAge = function () {
        this.age = today.getFullYear() - this.birthday.getFullYear();
        let m = today.getMonth() - this.birthday.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < this.birthday.getDate())) {
            this.age--;
        }
        return this.age;
    };

    this.getPassword = function () {
        this.passwordNew = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        return this.passwordNew;
    };
}

let userNew = new createNewUser();
console.log(`Your First Name: ${userNew.firstName}`);
console.log(`Your Last Name: ${userNew.lastName}`);
console.log(`Your Login: ${userNew.getLogin()}`);
console.log(`Your Age: ${userNew.getAge()}`);
console.log(`Your Password: ${userNew.getPassword()}`);