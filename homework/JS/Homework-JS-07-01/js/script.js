const massivElemets = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const listArray = massivElemets.map(list => `<li> ${list}</li>`).join('');
let separateAndAdd = `<ol> ${listArray} </ol>`;
document.getElementById('container').innerHTML = separateAndAdd;

let seconds = 10;
function timeSec() {
    if (seconds < 1) {
        document.body.innerHTML = '';
    }
    else {
        document.getElementById('timer').innerHTML = seconds;
        setTimeout(timeSec, 1000);
        seconds--;
    }
}
timeSec();
