let dataType = 'string';
let array = ['hello', 'world', 23, '23', null];
let newArray = filterBy(array, dataType);

console.dir(newArray);

function filterBy(array, dataType) {
    return array.filter(item => typeof item !== dataType);
}
