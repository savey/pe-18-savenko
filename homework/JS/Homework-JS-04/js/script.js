function createNewUser() {
    this.firstName = prompt('Enter First Name', '');
    while (this.firstName === ''){
        this.firstName = prompt('Enter First Name again','');
    }

    this.lastName = prompt('Enter Last Name', '');
    while (this.lastName === ''){
        this.lastName = prompt('Enter Last Name again','');
    }

    this.getLogin = function(){
        this.loginNew = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return this.loginNew;
    };
}

let userNew = new createNewUser();
console.log(`Your First Name: ${userNew.firstName}`);
console.log(`Your Last Name: ${userNew.lastName}`);
console.log(`Your Login: ${userNew.getLogin()}`);

