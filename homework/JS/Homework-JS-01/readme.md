# Обьяснить своими словами разницу между обьявлением переменных через var, let и const. #
        var - устаревший вариант задания переменных. Особенностью является то, что переменная заданная с помощью var
        будет доступна в любой части кода, при том что let и const можно задать переменную в блоке и она будет доступна
        только в этом блоке.
        let и const в отличии от var доступны только после их объявления. var же существует сразу и равна undefined.
        Также let при использовании в цикле при каждом итогеции цикла задается заново, а var в таком случае будет все
        время одна.
        Отличие let от const в том, что const задается один раз и потом ее невозможно изменить, то есть значение по сути
        является константой, при этом дут можно переприсваивать сколько угодно раз.

# Почему объявлять переменную через var считается плохим тоном? #
        Переменная var является устаревшей. В первую очередь из-за своих отличий от let описанных выше. Как пример
        распространенной пррблемы с переменной заданной черех var, это невозможность переприсваивания ее в цикле.
