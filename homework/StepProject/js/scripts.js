(function ($) {
    $(function () {
        $("ul.menuOurServices").on("click", "li:not(.active)", function () {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.sectionTabOurServices")
                .find("div.contentTabOurServices")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
        });
    });
})($);





$(document).ready(function () {
    $('.menuAmazingWork li a').click(function (event) {
        event.preventDefault();
        $('.menuAmazingWork li').removeClass('select');
        $(this).parent('li').addClass('select');
        let imgWidth = '278px';
        let imgHeight = '200px';
        let thisItem = $(this).attr('rel');
        if (thisItem !== "all") {
            getMoreImgsSecond();
            getMoreImgsThird();
            $('.itemAmazingWork li[rel=' + thisItem + ']').stop()
                .animate({
                    'width': imgWidth,
                    'height': imgHeight,
                    'opacity': 1,
                    'marginRight': 0,
                    'marginLeft': 0
                });
            $('.itemAmazingWork li[rel!=' + thisItem + ']').stop()
                .animate({
                    'width': 0,
                    'height': 0,
                    'opacity': 0,
                    'marginRight': 0,
                    'marginLeft': 0
                });
        } else {
            $('.itemAmazingWork li').stop()
                .animate({
                    'width': imgWidth,
                    'height': imgHeight,
                    'opacity': 1,
                    'marginRight': 0,
                    'marginLeft': 0,
                });
        }
    });
    $('.itemAmazingWork li img').animate({'opacity': 1}).hover(function () {
            $(this).animate({'opacity': 1});
        }, function () {
            $(this).animate({'opacity': 1});
        }
    );
});

let clickNumber = 0;
$('#buttonLoadMore').on('click', createNewImg);

function createNewImg() {
    $('.preloader').css('display', 'block');
    if (clickNumber === 0) {
        setTimeout(getMoreImgsSecond, 2000);
    } else {
        setTimeout(getMoreImgsThird, 2000);
    }
}

function getMoreImgsSecond() {
    $('.displayNone').removeClass('displayNone');
    $('.categoryItemsOneScreenSecond').attr('class', 'categoryItemsOneScreen');
    $('.categoryItemsOneScreen').css('display', 'block');
    $('.categoryItemsOneScreenSecond').removeClass('.categoryItemsOneScreenSecond');
    $('.contentAmazingWork').css('min-height', '850px');
    $('.contentAmazingWork').css('height', 'auto');
    $('.preloader').css('display', 'none');
    clickNumber++;
}

function getMoreImgsThird() {
    $('#buttonLoadMore').remove();
    $('.displayNone').removeClass('displayNone');
    $('.categoryItemsOneScreenThird').attr('class', 'categoryItemsOneScreen');
    $('.categoryItemsOneScreen').css('display', 'block');
    $('.categoryItemsOneScreenThird').removeClass('.categoryItemsOneScreenThird');
    $('.contentAmazingWork').css('min-height', '850px');
    $('.contentAmazingWork').css('height', 'auto');
    $('.preloader').css('display', 'none');
}






$('.classForSlick').slick({
    arrows: false,
    speed: 300,
    asNavFor:'.classForSlick2'
});

$('.classForSlick2').slick({
    centerMode: true,
    prevArrow: '.sliderLeft',
    nextArrow: '.sliderRight',
    speed: 300,
    asNavFor:'.classForSlick',
    focusOnSelect: true,
    variableWidth: true
});
